#!/usr/bin/env python3

"""
"""

__author__ = "Dennis Scheper, Marco van Til, Wytze Bekker en Benjamin Berkhout"
__date__ = "31-01-2019"
__version__ = "v1.0.0"

import os
from subprocess import Popen

global done

def fastqc_run(fastq_path, output_dir, options=['--extract',]):
    """
    Run the fastqc program on a specified fastq file and return the output directory path.

    Parameters
    ----------
    fastq_path : str
        Path to the fastq file to analyze.
    output_dir : str
        Directory where output will be written. It will be created if it does not exist.
    options : list of str (optional)
        Command-line flags and options to fastqc. Default is --extract.

    Returns
    -------
    output_dir : str
        The path to the directory containing the detailed output for this fastq file.
        It will be a subdirectory of the specified output dir.
    """
    # Fastqc creates a directory derived from the basename
    fastq_dir = os.path.basename(fastq_path)
    if fastq_dir.endswith(".gz"):
        fastq_dir = fastq_dir[0:-3]
    if fastq_dir.endswith(".fq"):
        fastq_dir = fastq_dir[0:-3]
    if fastq_dir.endswith(".fastq"):
        fastq_dir = fastq_dir[0:-6]
    fastq_dir = fastq_dir + "_fastqc"
    # Delete the zip file and keep the uncompressed directory
    zip_file = os.path.join(output_dir, fastq_dir + ".zip")
    #os.remove(zip_file)
    output_dir = os.path.join(output_dir, fastq_dir)
    return output_dir

def bowtie2_run(bowtie_path, reference_genome_dir, output_dir):
    done = 0
    process = Popen(["bowtie2 -x {} -U {} -S {}".format(reference_genome_dir, fast_qc_path, output_dir)])
    while process.poll() == None:
        None
    else:
        done = 1

def samtools_run(bam_file_name, sam_file_name):
    done = 0
    process = Popen(["samtools view -Sb -o {} -A 4 {}".format(bam_file_name, sam_file_name)])
    while process.poll() = None:
        None
    else:
        done = 1

def picard_add_or_replace_read_groups_run(bam_sorted, bam_sorted_groups):
    done = 0
    process = Popen(["""picard-tools AddOrReplaceReadGroups INPUT={} OUTPUT={} LB=unknown PU=unknown
                     SM=unknown PL=illumina CREATE_INDEX=true""".format(bam_sorted, bam_sorted_groups)])
    while process.poll() = None:
        None
    else:
        done = 1

def picard_fix_mate_information_run(bam_sorted_groups):
    done = 0
    process = Popen(["picard-tools FixMateInformation INPUT={}".format(bam_sorted_groups)])
    while process.poll() = None:
        None
    else:
        done = 1

def picard_mark_duplicates_run(bam_sorted_groups, bam_marked):
    done = 0
    process = Popen(["picard-tools MarkDuplicates INPUT={} OUTPUT={} CREATE_INDEX=true".format(bam_sorted_groups, bam_marked)])
    while process.poll() = None:
        None
    else:
        done = 1

def samtools_sort_run(bam_marked, bam_sorted):
    done = 0
    process = Popen(["samtools sort -n {} -o {}".format(bam_marked, bam_sorted)])
    while process.poll() = None:
        None
    else:
        done = 1

def featurecounts_run(reference_GTF):
    done = 0
    process = Popen(["featureCounts -a {} -o {}".format(reference_GTF, "feature_output")])
    while process.poll() = None:
        None
    else:
        done = 1

def run_R(dir_to_featurecounts_file):
    done = 0
    process = Popen(["pca_plot.R"])
    while process.poll() = None:
        None
    else:
        done = 1

def main():
    Nl_cohort =  ['1_R06009_S11_R1_001.fastq', '2_R02086_S10_R1_001.fastq', '3_R09048_S9_R1_001.fastq',
                  '4_R07012_S8_R1_001.fastq', '5_R07015_S7_R1_001.fastq', '6_R98003_S6_R1_001.fastq',
                  '7_R11065_S5_R1_001.fastq', '8_R12102_S4_R1_001.fastq', '9_R03047_S3_R1_001.fastq',
                  '10_R12041_S2_R1_001.fastq', '11_R12041_S1_R1_001.fastq', '12_R09096_S12_R1_001.fastq',
                  '13_R09096_S13_R1_001.fastq', 'R09121_014_S1_R1_001.fastq', 'R08072_015_S2_R1_001.fastq',
                  'R11049_016_S3_R1_001.fastq', 'R01045_017_S4_R1_001.fastq', 'R09105_018_S5_R1_001.fastq',
                  'R12008_019_S6_R1_001.fastq', 'R07060_020_S7_R1_001.fastq', 'R06036_021_S8_R1_001.fastq',
                  'EAW_022_S9_R1_001.fastq', 'R95036_023_S10_R1_001.fastq', 'R13160_024_S11_R1_001.fastq',
                  'R13160_025_S12_R1_001.fastq', 'R05013_026_S13_R1_001.fastq', 'R05013_027_S14_R1_001.fastq']
    BA_cohort_1 = ['9017_S6_L001_R1_001.fastq.gz', '9017_S6_L001_R2_001.fastq.gz',
                   '95026WB_S18_L001_R1_001.fastq.gz', '95026WB_S18_L001_R2_001.fastq.gz',
                   '95026_S16_L001_R1_001.fastq.gz', '95026_S16_L001_R2_001.fastq.gz',
                   '96024_S7_L001_R1_001.fastq.gz', '96024_S7_L001_R2_001.fastq.gz',
                   'D48_S8_L001_R1_001.fastq.gz', 'D48_S8_L001_R2_001.fastq.gz',
                   'R00063_S14_L001_R1_001.fastq.gz', 'R00063_S14_L001_R2_001.fastq.gz',
                   'R03057_S19_L001_R1_001.fastq.gz', 'R03057_S19_L001_R2_001.fastq.gz',
                   'R04027WB_S17_L001_R1_001.fastq.gz', 'R04027WB_S17_L001_R2_001.fastq.gz',
                   'R04027_S21_L001_R1_001.fastq.gz', 'R04027_S21_L001_R2_001.fastq.gz',
                   'R04067WB_S11_L001_R1_001.fastq.gz', 'R04067WB_S11_L001_R2_001.fastq.gz',
                   'R04067_S1_L001_R1_001.fastq.gz', 'R04067_S1_L001_R2_001.fastq.gz']
    BA_cohort_2 = ['R95007_S10_L001_R1_001.fastq.gz', 'R95007_S10_L001_R2_001.fastq.gz',
                   'R97031_S20_L001_R1_001.fastq.gz', 'R97031_S20_L001_R2_001.fastq.gz',
                   'Ri303103_S12_L001_R1_001.fastq.gz', 'Ri303103_S12_L001_R2_001.fastq.gz',
                   'Ri306029_S13_L001_R1_001.fastq.gz', 'Ri306029_S13_L001_R2_001.fastq.gz']

    for file in Nl_cohort:
        fastqc_run('~/../../commons/Themas/Thema06/Themaopdracht/macaque_story/RNA_Microglia/MacaqueNL/fastqFiles/{}'.format(file),
            '~/Desktop/eindopdracht/NL')

    for file in BA_cohort_1:
        fastqc_run('~/../../commons/Themas/Thema06/Themaopdracht/macaque_story/RNA_Microglia/MacaqueBrazil/fastqFiles/{}'.format(file),
            '~/Desktop/eindopdracht/Brazil')
    for file in BA_cohort_2:
        fastqc_run('~/../../commons/Themas/Thema06/Themaopdracht/macaque_story/RNA_Microglia/MacaqueBrazil/fastqFiles/{}'.format(file),
            '~/Desktop/eindopdracht/Brazil')
       
    for file in os.listdir("eindopdracht/NL"):
        bowtie2_run(file, '~/Desktop/eindopdracht/reference_genome.fni', '~/Desktop/eindopdracht/samNL/{}'.format(file))

    for file in os.listdir("eindopdracht/Brazil"):
        if done = 1:
            bowtie2_run(file, '~/Desktop/eindopdracht/reference_genome.fni', '~/Desktop/eindopdracht/samBRA/{}'.format(file))

    for file in os.listdir('eindopdracht/samBRA'):
        if done = 1:
            samtools_run(file, '~/Desktop/eindopdracht/bamBRA/{}'.format(file))

    for file in os.listdir('eindopdracht/samNL'):
        if done = 1:
            samtools_run(file, '~/Desktop/eindopdracht/bamNL/{}'.format(file))

    for file in os.listdir('eindopdracht/bamBRA'):
        if done = 1:
            picard_add_or_replace_read_groups_run(file, '~/Desktop/eindopdracht/addorreplace_BRA/{}'.format(file))
        
    for file in os.listdir('eindopdracht/addorreplace_BRA'):
        if done = 1:
            picard_fix_mate_information_run(file, '~/Desktop/eindopdracht/fixmate_BRA/{}'.format(file))

    for file in os.listdir('eindopdracht/fixmate_BRA'):
        if done = 1:
            picard_mark_duplicates_run(file, '~/Desktop/eindopdracht/bamBRA_compleet/{}'.format(file))

    for file in os.listdir('eindopdracht/bamNL'):
        if done = 1:
            picard_add_or_replace_read_groups_run(file, '~/Desktop/eindopdracht/addorreplace_NL/{}'.format(file)))

    for file in os.listdir('eindopdracht/addorreplace_NL'):
        if done = 1:
            picard_fix_mate_information_run(file, '~/Desktop/eindopdracht/fixmate_NL/{}'.format(file))

    for file in os.listdir('eindopdracht/fixmate_NL'):
        if done = 1:
            picard_mark_duplicates_run(file, '~/Desktop/eindopdracht/bamNL_compleet/{}'.format(file))

    for file in os.listdir('eindopdracht/bamNL_complete'):
        if done = 1:
            samtool_sort_run()

    for file in os.listdir('eindopdracht/bamBRA_complete'):
        if done = 1:
            samtool_sort_run()

    for file in os.listdir('eindopdracht/samtoolNL_sorted'):
        if done = 1:
            featurecounts_run('~Desktop/eindopdracht/reference_featurecounts/')

    for file in os.listdir('eindopdracht/featurecounts'):
        if done = 1:
            run_R()
    
if __name__ == "__main__":
    main()

